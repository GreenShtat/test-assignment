using System.Collections.Generic;
using System.Linq;
using TestAssignment.BL.Abstraction;
namespace TestAssignment.BL.Impl
{
    public class ElementMerger: IElementMerger
    {
        private static ElementMerger _instance;
        private ElementMerger()
        {}
        public IEnumerable<IElement> MergeElements(IEnumerable<IElement> elements, IElement newElement)
        {
            elements = elements.OrderBy(i => i.Number);
            var listElements = new List<IElement>();
            var lastIndexListElements = 0;
            foreach (var element in elements)
            {
                listElements.Add(element);
                if (listElements[lastIndexListElements].Number == newElement.Number)
                {
                    listElements.RemoveAt(lastIndexListElements);
                    listElements.Add(newElement);
                    listElements.Add(new Element(++element.Number, element.Body));
                }
                if (listElements[lastIndexListElements].Number == element.Number && lastIndexListElements !=0)
                {
                    element.Number++;
                }
                lastIndexListElements++;
            }
            return listElements;
        }
        public static ElementMerger Get()
        {
            if(_instance == null)
                _instance = new ElementMerger();
            return _instance;
        }
    }
}